#include "commandnotfound.h"
#include "ui_commandnotfound.h"

CommandNotFound::CommandNotFound(QWidget *parent) :
    QMainWindow(parent),
    m_ui(new Ui::CommandNotFound)
{
    m_ui->setupUi(this);
}

CommandNotFound::~CommandNotFound() = default;
