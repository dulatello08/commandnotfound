#ifndef COMMANDNOTFOUND_H
#define COMMANDNOTFOUND_H

#include <QMainWindow>
#include <QScopedPointer>

namespace Ui {
class CommandNotFound;
}

class CommandNotFound : public QMainWindow
{
    Q_OBJECT

public:
    explicit CommandNotFound(QWidget *parent = nullptr);
    ~CommandNotFound() override;

private:
    QScopedPointer<Ui::CommandNotFound> m_ui;
};

#endif // COMMANDNOTFOUND_H
